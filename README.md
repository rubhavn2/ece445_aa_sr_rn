# ECE 445 Senior Design Project - Hand Crank Charger (SP23 - 14)

Welcome to the GitLab repository for our ECE 445 Senior Design Project. 

The **team members** for this project are:
- Achyut Agarwal (achyuta2)
- Shreyasi Ray (ray17)
- Rubhav Nayak (rubhavn2)

We are creating a Hand Cranked Portable Device Charger primarily to be used by a person when their phone is really low on battery, and they are not going to be near a power outlet for quite some time (long flights, camping, etc.)

In this repository we will keep a track of all the images, documents and our **lab notebooks** that we will use during this project!

If you need to contact us, feel free to email us at netID@illinois.edu where you can use our netIDs that are in parentheses above. 

Please Note: This repository is a part of a senior design project at the University of illinois at Urbana Champaign

