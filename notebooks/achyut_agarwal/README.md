## 01/17/2023
Team met up virtually to discuss different ideas to be posted for our project. 
I proposed a [LiDAR-assisted device for helping Visually Impaired people. ](https://courses.engr.illinois.edu/ece445/pace/view-topic.asp?id=71714)
We really liked the idea and I posted it that day itself and we discussed the practicality as well as the presence of competitors. 
We also discussed the other ideas of the hand cranks charger and the game controller ideas. 

## 01/22/2023
The team met up in person to finalize which project we are going for and submitted our RFA to hopefully get it approved before the early deadline. We went with the [Hand Crank Energy Source Idea](https://courses.engr.illinois.edu/ece445/pace/view-topic.asp?id=72429). We discussed the parts of the project and made the RFA. We discussed all the different parts of the project, how to make it efficient, and how to also make it cost-effective. 



## 02/04/2023
We met up to discuss the Project Proposal. This was more hands-on as we needed to discuss the exact parts of the project and create the block diagram. We came up with a rough Block Diagram (image attached) and we also discussed the key points of our proposal today.

![Rough Block Diagram](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/blob/main/notebooks/achyut_agarwal/images/rough_block.png "Rough Block Diagram")

## 02/07/2023
Today we had our TA meeting for the first time, we discussed the block diagram in detail, we also discussed pros and cons of which motor we are using, and discussed the variable voltage divider. This was because the Variable Voltage Divider is an extremely critical part of our project, and making that work will be a large part of our project. 
These were some of the notes we took during the quick discussion. 

![TA Meeting Notes](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/blob/main/notebooks/achyut_agarwal/images/TA_meeting.png "TA Meeting Notes")

## 02/09/2023
We met up to finalize the project proposal, we created the CAD models for the rough design of our product, and also further discussed intricacies in the design.


## 02/10/2023
We went to the machine shop and met them to discuss the needs for our project. We also discussed motor choices and which motors would be more viable than others. We have decided on a 12V Brushed motor as it provides a good output, but is not impossible to crank at roughly 120rpm. We also have thought of the rough design of our project. 

## 02/13/2023
We met Jack Blevins today to discuss our project, he brought up some very useful points regarding the hand crank placement and also using the 12V motor to charge the battery and the device simultaneously. This would help prevent unecessary power wastage as well. 
