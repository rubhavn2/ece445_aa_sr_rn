Tuesday - 2/7/2023
 - Made Block Diagram
 - Spoke with TA about our project, advice to look into regulator control system as well as boost converter

Thursday - 2/9/2023
 - Met with team to discuss Project Proposal
 - Modified Block Diagram to reflect the suggestions of TA
 - Wrote up on the Tolerances, Safety and Designed High-level Visual Aid for the proposal.

Friday - 2/10/2023
 - Spoke with Machine Shop
 - Advice to use a bigger, more reliable 12V geared motor to get our desired results.
 - Discussed with teammates and decided we would go with this for the sake of reliability since our project mainly consists of the electronics beyond the motor

Monday - 2/13/2023
 - Spoke with Jack Blevins, our mentor for this project.
 - He suggested that we go with the Machine Shop's advice of using the bigger motor
 - He also suggested a design where the hand crank is at the top of the product instead of on the side.

Thursday - 2/16/2023
 - Edited Block Diagram to reflect the suggestions of Jack
 
