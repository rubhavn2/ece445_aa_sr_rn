# Progress Notebook

This personal notebook keeps track of the progress of the team in regards to the project, and every aspect is kept track of through this file. 

## 17th January, 2023

The team discussed various ideas, and each member wrote out points for each idea in order to formally be able to write out the initial post on the Web Board. 

My idea was the Hand-Cranked Charger, since it seemed like a very useful concept to have in times of emergency. It would allow users to save themselves in dire situations, and they would be able to generate power for their own device and make emergency phone calls or send texts as per their requirements. 

The team liked the idea and we went through with the initial post in order to discuss the idea further.

[Link to Initial Post for this idea](https://courses.engr.illinois.edu/ece445/pace/view-topic.asp?id=71835)

## 22nd January, 2023

The team had a meeting, and we decided which idea to post for formal approval, that is, the RFA. We took into consideration the advice given by the Professor and the TAs through replies to the initial posts. 

Two of our ideas were approved for the RFA stage, and the team discussed each thoroughly to check which idea is better to work on, and more viable. We talked about efficiency, affordability and other features of each product, along with design factors of the product itself. We were able to post our RFA and get it approved before the early deadline given. 

[Link to RFA for the idea](https://courses.engr.illinois.edu/ece445/pace/view-topic.asp?id=72429)

## 4th February, 2023

The team had a meeting to discuss the Project Proposal. After checking the requirements, we worked on the details of the block diagram, high-level requirements and the general introduction. 

We had to start considering the design element of the product in greater detail since the proposal would give a deeper view into the workings of the product. 

The general rough idea of the block diagram is attached.

![Rough Block Diagram](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/blob/main/notebooks/shreyasi_ray/images/Screen_Shot_2023-02-07_at_5.50.14_PM.png)

## 7th February, 2023

The team had the first meeting with a TA, and during this short discussion we learned about many things we needed to include while writing out the project proposal. 

We talked about various motors which would be suitable, while mentioning which portions of the product we wish to manually create ourselves. While giving brief descriptions of each of our components through the block diagram, we had a helpful discussion about the variable voltage divider. This is one of the most crucial portions of our design, and we realized we had to do more critical thinking about the high-level requirements before the project proposal was written up. 

We got our assigned locker, and discussed setting up future meetings with our designated TA.

## 9th February, 2023

The team had a meeting to discuss and finalize the project proposal before the deadline. We were able to work and visalize the CAD models for the design of the hand-crank generator product. We had to go through manyh rough ideas for feasible designs for the product.  Several components were discussed so that we could later get confirmation on the models and whether we would be able to make such a product design.

## 10th February, 2023

The team went to the machine shop to discuss the physical components of the product. We learnt about the various motors, and which sizes would be viable for the design. 

Since we did not want the generator to be very big, we asked for more portable size options and were given many suggestions as to what motors we could use. The machine shop lent us a motor to conduct testing on, and sketch out the realistic area and size of the product. The machine shop also helped us get ideas on how big the casing should be for the design to be viable. We were able to think of the rough placement of components in the casing, to make the most use of the main board which will have the main components mounted on it.
We were able to choose a 12V brushed motor since it provides the required output and it could be cranked at a reasonable rate. There was also a discussion about the rough size of the hand-crank, which would have to be long enough for efficient cranking. This meeting was able to give us our checkpoints for further discussion. To avoid electrocution and unwanted conduction, the machine shop also showed us plastic casings to help with safety regulations of the product. The rough components of the design was put together through this meeting, and it helped get an idea on what is viable for the design and what is not.



